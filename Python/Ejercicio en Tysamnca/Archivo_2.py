#

# para aplicar OR | , Y para aplicar AND & , si no se coloca se usa por defecto (Solo al principio)

    self.search(['|'('is_company','=',True),('customer','=',True)]) #para OR

    self.search(['%'('is_company','=',True),('customer','=',True)]) #para AND


# (a AND b) OR c
    self.search(['|','&'('is_company','=',True),('customer','=',True),('customer','=',True)]) #Usando ambas, se ejecutan los dos primeros o
                                                                                         #operandos con AND, y luego el resultado de ese se aplica un OR con el tercer operando


# (a AND b) OR ((((c AND d) AND e) AND f))

    domain = ['|', '&', (True, '=', True), ('fb_id', '=', fb_brw.id),
                      '&', '&', '&', ('date_invoice', '>=', local_period.get('dt_from')),
                      ('date_invoice', '<=', local_period.get('dt_to')),
                      ('type', 'in', inv_type), ('state', 'not in', inv_state)]


##Metodos ORM

#Metodo Create

    self.create({'name':'nuevo'}) #Crea un registro nuevo o fila en la tabla de la base de datos
    res.parnet(12)

#Metodo Write

    self.write({key:value})

    obj.write({key:value})

    self.env['model_name'].write([id],{key:value}) # con el id se le indica el registro de la tabla que se quiere modificar

    obj.search(domain).write({key:value}) #se busca en el modelo lo que se quiere con el filtro de la variable domain, y luego se sobre escribe en los valores obtenido en la busqueda.

#Metodo exists

    record.exists() # Valida si el objeto tiene valores en la tabla, para validar si fueron borrados anteriormente

#Metodo Unlink

    record.unlink() # se usa para borra registros de una tabla de manera definitiva

#Metodo fields_get

    fields_get() #


#Metodo fields_view_get

    fields_view_get() #Se usa para cargar los valores y la vista en el navegador para luego imprimirla

#Metodo default_get(fields)

    default_get(fields)

#Metodo name_get()

    name_get() # Se usa para pintar el nombre de un registro de un many2one, por ejemplo el nombre pinta el nombre de una factura!

     #Otra forma de escribirlo si el campo name_get no esta definido, se declara el siguiente comando:

      _rec_name = "campo"

  #SE DEBE REALIZAR UNA PRUEBA DE COLOCAR VARIOS name_get PARA DIFERENTES CAMPOS


#Metodo name_create()

    name_create() #Crea un nuevo registro y le asigna un nombre
