#Enunciado
""" Ingles: Have the function LetterChanges(str) take the str parameter being passed and modify 
            it using the following algorithm. Replace every letter in the string with the letter 
            following it in the alphabet (ie. c becomes d, z becomes a). Then capitalize every vowel 
            in this new string (a, e, i, o, u) and finally return this modified string.
"""
""" Español: Haga que la función LetterChanges (str) tome el parámetro str que se pasa y modifique
            utilizando el siguiente algoritmo. Reemplaza cada letra en la cadena con la letra
            siguiéndolo en el alfabeto (es decir, c se convierte en d, z se convierte en a). Luego capitaliza cada vocal
            en esta nueva cadena (a, e, i, o, u) y finalmente devuelva esta cadena modificada.
"""
""" Ejemplos:   Input:"hello*3"
                Output:"Ifmmp*3"

                Input:"fun times!"
                Output:"gvO Ujnft!"
"""

def LetterChanges(str): 
    #entrada = "hello*3"
    abec = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
    palabra = []
    for x in str:
        if x.isalpha():
            for y in range(len(abec)):
                if x == abec[y]:
                    if x == "z":
                        palabra.append(abec[0])
                    elif abec[y+1] in 'aeiou':
                        palabra.append(abec[y+1].upper())
                    else:
                        palabra.append(abec[y+1])  
        else:
            palabra.append(x)  
    """ Otra forma de modificar las vocales luego de tener todo el contenido
    #for xys in range(len(palabra)):
    #    if palabra[xys] in 'aeiou':
    #        palabra[xys] = palabra[xys].upper()    
    """        
    str = "".join(palabra)
    return str

print (LetterChanges(input()))
