#Enunciado
""" Ingles: Have the function FirstFactorial(num) take the num parameter being passed
            and return the factorial of it. For example: if num = 4, then your program 
            should return (4 * 3 * 2 * 1) = 24. For the test cases, the range will be 
            between 1 and 18 and the input will always be an integer. 
"""

""" Español: Haga que la función FirstFactorial ( num ) tome el parámetro num que se 
             pasa y devuelva su factorial. Por ejemplo: si num = 4, entonces su programa
             debería regresar (4 * 3 * 2 * 1) = 24. Para los casos de prueba, el rango
             estará entre 1 y 18 y la entrada siempre será un número entero.  
"""
""" Ejemplos:   Input:4
                Output:24

                Input:8
                Output:40320
"""

def FirstFactorial(num): 
    var = 1
    for n in range(1,num+1):
        var = (n * var)
    return var
print (FirstFactorial(int(input())))