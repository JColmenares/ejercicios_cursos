#Enunciado
""" Ingles: Have the function FirstReverse(str) take the str parameter being passed 
            and return the string in reversed order. For example: if the input string is 
            "Hello World and Coders" then your program should return the string 
            sredoC dna dlroW olleH. 
"""

""" Español: Haga que la función FirstReverse ( str ) tome el parámetro str que se 
             está pasando y devuelva la cadena en orden inverso. Por ejemplo: 
             si la cadena de entrada es "Hello World and Coders", su programa debería 
             devolver la cadena sredoC dna dlroW olleH . 
"""
""" Ejemplos:   Input:"coderbyte"
                Output:"etybredoc"

                Input:"I Love Code"
                Output:"edoC evoL I
"""

def FirstReverse(str): 
    return str[::-1]
     
print (FirstReverse(input()))