class Persona():

    def __init__(self,nombre,edad,Lugar_residencia):
        self.nombre = nombre
        self.edad = edad
        self.lugar_residencia = Lugar_residencia

    def descripcion(self):
        print ("Nombre:", self.nombre, "Edad:", self.edad, "Residencia:", self.lugar_residencia)

class Empleado(Persona):

    def __init__(self,salario,antiguedad,nombre_empleado,edad_empleado,residencia_empleado):
        
        super(Empleado, self).__init__(nombre_empleado,edad_empleado,residencia_empleado) #TODO# Sirve para ejecutar una clase cuando se presenta herencia multiple
        
        self.salario = salario
        self.antiguedad = antiguedad

    def descripcion(self):

        super(Empleado, self).descripcion()

        print ("Salario:", self.salario, "Antiguedad:", self.antiguedad )


Antonio = Empleado(1500,15,"Antonio", 55, "España")

Antonio.descripcion()

print(isinstance(Antonio,Persona)) #TODO# Se usa para validar si el Objeto pertenece a una Clase

