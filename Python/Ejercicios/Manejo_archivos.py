from io import open

archivo_texto = open("archivo.txt","r+") # "w" : Modo Escritura, "r" : Modo lectura, "a" : Modo extención y "r+" : Modo Lectura y Escritura

#archivo_texto.write("Esto es otro cambio ")
#archivo_texto.seek(11)
#print(archivo_texto.read())

lis_tex = archivo_texto.readlines()

lis_tex[1] = "Se a cambiado esta linea completa"

archivo_texto.seek(0)

archivo_texto.writelines(lis_tex)

archivo_texto.close()