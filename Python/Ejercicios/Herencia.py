class Vehiculo():

    def __init__(self, marca,modelo):
        self.marca = marca
        self.modelo = modelo
        self.enmarcha = False
        self.acelera = False
        self.frena = False

    def arrancar(self):
        self.enmarcha = True

    def acelerar(self):
        self.acelera = True

    def frenar(self):
        self.frena = True

    def estado(self):
        print ("Marca: ", self.marca, "\nModelo: ", self.modelo, "\nEn Marcha: ", self.enmarcha, "\nAcelerando: ", self.acelera,
               "\nFrenando: ", self.frena)

class Furgoneta(Vehiculo):

    def carga(self,cargar):
        self.cargado = cargar
        if(self.cargado):
            return "La furgoneta está cargada"
        else:
            return "La furgoneta no está cargada"

class VElectricos():

    def __init__(self):
        self.autonomia = 100

    def cargarEnergia(self):
        self.cargando = True

class Moto(Vehiculo):
    hcaballito = ""

    def caballito(self):
        self.hcaballito = "Voy haciendo el caballito"

    def estado(self):
        print ("Marca: ", self.marca, "\nModelo: ", self.modelo, "\nEn Marcha: ", self.enmarcha, "\nAcelerando: ", self.acelera,
               "\nFrenando: ", self.frena, "\n", self.hcaballito)

# TODO # NOTA: Si se crea un objeto con la clase moto que hereda Vehiculo, y se llama el metodo caballito se sobre escribe
# TODO #       el estado de la clase principal permitiendo mostrar el nuevo estado con la nueva condición

MiMoto = Moto("Honda","CBR")
MiMoto.caballito()
MiMoto.estado()

print ("\n")

MiFurgoneta = Furgoneta("Renault","Kangoo")
MiFurgoneta.arrancar()
MiFurgoneta.estado()
print(MiFurgoneta.carga(True))

class BicicletaElectrica(VElectricos,Vehiculo): #TODO # Al hacer Herencias multiples Se le da prioridad a los argumentos del contructor de la primera Clase Heredada
    pass

miBici = BicicletaElectrica()

